/*
    Copyright (C) 2003-2010 Fons Adriaensen <fons@kokkinizita.net>
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/


// --------------------------------------------------------------------------------


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <jack/jack.h>
#include "rngen.h"



static jack_client_t  *jack_handle;
static jack_port_t    *jack_out1;
static jack_port_t    *jack_out2;
static Rngen          rngen;
static float          b0, b1, b2, b3, b4, b5, b6;
static float          gain = 1.0f;


static void process (int n, float *op1, float *op2)
{
    float x;

    while (n--)
    {
	x = gain * rngen.grandf ();
	*op1++ = 0.07071f * x;       	   
        x *= 0.023f;
        b0 = 0.99886f * b0 + 0.0555179f * x;
        b1 = 0.99332f * b1 + 0.0750759f * x;
        b2 = 0.96900f * b2 + 0.1538520f * x;
        b3 = 0.86650f * b3 + 0.3104856f * x;
        b4 = 0.55000f * b4 + 0.5329522f * x;
        b5 = -0.7616f * b5 - 0.0168980f * x;
        *op2++ = b0 + b1 + b2 + b3 + b4 + b5 + b6 + x * 0.5362f;
        b6 = x * 0.115926f;
    }
}


static void jack_shutdown (void *arg)
{
    exit (1);
}


static int jack_callback (jack_nframes_t nframes, void *arg)
{
    float *op1, *op2;

    op1 = (float *)(jack_port_get_buffer (jack_out1, nframes));
    op2 = (float *)(jack_port_get_buffer (jack_out2, nframes));
    process (nframes, op1, op2);
    return 0;
}


int main (int ac, char *av [])
{
    float           g;
    jack_options_t  opts;
    jack_status_t   stat;

    if (ac > 1)
    {
        g = atof (av [1]);
        if (g > -10) 
        {
	    fprintf (stderr, "Level is too high, max = -10dB\n");
	    return 1;
        }
        gain = powf (10.0f, 0.05f * (g + 20));
    }
    
    opts = JackNoStartServer;
    if ((jack_handle = jack_client_open ("jnoise", opts, &stat)) == 0)
    {
        fprintf (stderr, "Can't connect to JACK\n");
        return 1;
    }

    jack_set_process_callback (jack_handle, jack_callback, 0);
    jack_on_shutdown (jack_handle, jack_shutdown, 0);
    jack_out1 = jack_port_register (jack_handle, "white", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    jack_out2 = jack_port_register (jack_handle, "pink",  JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);

    rngen.init (0);

    if (jack_activate (jack_handle))
    {
        fprintf(stderr, "Can't activate JACK");
        return 1;
    }

    while (1) usleep (200000);
    return 0;
}


// --------------------------------------------------------------------------------
